<?php
// Plugin Menu
// ***********************

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Pixelpark.' . $_EXTKEY,
    'OptivoResultRenderer',
    array(
        'Optivo' => 'resultOptivo',
    ),
    // non-cacheable actions
    array(
        'Optivo' => 'resultOptivo',
    )
);
