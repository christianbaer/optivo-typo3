<?php

namespace Pixelpark\Optivo\Service;

use Pixelpark\Optivo\Exception\LoginFailedException;
use Pixelpark\Optivo\Library\BroadmailApiSoap;
use SoapFault;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class OptivoConnection extends BroadmailApiSoap {

    private $sessionId = FALSE;
    private $recipientListId = 0;
    private $language = '';
    private $optinProcessId = 0;
    private $topicsAttributes = array();
    private $error = FALSE;

    const DEFAULT_RECIPIENT_LISTID = 0;
    const DEFAULT_OPTIN_PROCESSID = 0;
    const RECIPIENT_ADD_SUCCESS = 0;
    const RECIPIENT_ADD_VALIDATION_ERROR = 1;
    const RECIPIENT_ADD_ON_CANCELLIST = 2;
    const RECIPIENT_ADD_ON_RESTRICLIST = 3;
    const RECIPIENT_ADD_BOUNCE_LIMIT = 4;
    const RECIPIENT_ADD_ALREADY_INLIST = 5;
    const RECIPIENT_ADD_WAS_FILTERD = 6;
    const RECIPIENT_ADD_COMMON_ERROR = 7;
    const RECIPIENT_UPDATE_ATTRIB_OVERFLOW = 8;
    const MAX_TOPIC_ATTRIBUTE_LENGTH = 255;
    const TOPIC_ATTRIB_BASE_STR = 'Interesse';
    const UUID_SALT1 = 'OPTIVO-N-MDA-UUID';
    const UUID_SALT2 = 't295720a90314785d50694d86df86ea4';
    const SUBSCRIBE_IP_ATTR = 'SubscribeIP';
    const SUBSCRIBE_DATE_ATTR = 'Subscribedate';
    const SUBSCRIBE_URL_LABEL = 'Subscribe URL';
    const SUBSCRIBE_URL_ATTR = 'subscribeurl';

    /**
     * Webservice constructor creates session and sets properties
     *
     * @param int $mandatorId
     * @param string $userName
     * @param string $password
     * @param int $recipientListId
     * @param int $optinProcessId
     */
    function __construct($mandatorId, $userName, $password, $recipientListId = 0, $optinProcessId = 0) {

        // login on constructor
        try {
            $this->sessionId = $this->SessionWebservice()
                ->login($mandatorId, $userName, $password);
        } catch (SoapFault $exception) {
            $this->error = $exception->getMessage();
            $this->sessionId = FALSE;
        }

        // with valid connection set properties
        if ($this->isValid()) {
            $this->setRecipientListId($recipientListId);
            $this->setOptinProcessId($optinProcessId);
            $this->language = 'de';
        }else{
            $errorMessage = "Login failed; optivo-webservice (SessionWebservice) returned no SessionID; mandatorId=$mandatorId; userName=$userName";
            throw new LoginFailedException($errorMessage);
        }
    }

    /**
     * Destructor only does logout.
     */
    function __destruct() {
        if ($this->isValid()) {
            $this->SessionWebservice()->logout($this->sessionId);
        }
    }

    /**
     * Returns error message or FALSE if none
     *
     * @return boolean | string
     */
    public function getErrorMessage() {
        return $this->error;
    }

    /**
     * Getter method for OptinProcessId
     *
     * @return int
     */
    public function getOptinProcessId() {
        return $this->optinProcessId;
    }

    /**
     * Setter method for OptinProcessId
     *
     * @param int $optinProcessId
     */
    public function setOptinProcessId($optinProcessId) {
        // alternative method would be: pick first
        // $optinProcessId = array_pop($this->OptinProcessWebservice()
        //   ->getIds($this->sessionId));
        if (!$optinProcessId) {
            $this->optinProcessId = self::DEFAULT_OPTIN_PROCESSID;
        } else {
            $this->optinProcessId = $optinProcessId;
        }
    }

    /**
     * @return int
     */
    public function getRecipientListId() {
        return $this->recipientListId;
    }

    /**
     * @param int $recipientListId
     */
    public function setRecipientListId($recipientListId) {
        // alternative methods would be
        // pick first:
        //$this->recipientListId = $this->RecipientListWebservice()
        //  ->getAllIds($this->sessionId)[0];
        // or
        // pick by name:
        // fetch $this->RecipientListWebservice()->getDataSetFlat($this->getSessionId());
        // filter by second attribute in row
        if (!$recipientListId) {
            $this->recipientListId = self::DEFAULT_RECIPIENT_LISTID;
        } else {
            $this->recipientListId = $recipientListId;
        }
    }

    /**
     * Getter method for sessionId
     *
     * @return bool|string
     */
    public function getSessionId() {
        if ($this->isValid()) {
            return $this->sessionId;
        } else {
            return FALSE;
        }
    }

    /**
     *
     * Method to add a recipient with attributes recipientList
     *
     * @param string $emailAddress
     * @param string[] $attributes
     * @return int
     */
    public function addRecipientToList($emailAddress, $attributes = array(), $topicNames) {
        $status = self::RECIPIENT_ADD_COMMON_ERROR;
        $recipientId = $this->getRecipientId($emailAddress);
        $attributeNames = array_keys($attributes);
        $attributeValues = array_values($attributes);
    
        // test if recipient exists in list it can be updated
        if ($this->RecipientWebservice()
            ->contains($this->sessionId, $this->recipientListId, $recipientId)
        ) {
            $this->addRecipientTopic($emailAddress, $topicNames);
            $this->setAttributes($emailAddress, $attributes);
            // for privacy protection same return code as new add.
            $status = self::RECIPIENT_ADD_SUCCESS;
        }

        // if new recipient try to add it
        else {
            $status = $this->addNewRecipientToList($recipientId, $emailAddress, $attributeNames, $attributeValues, $topicNames);
        }

        $this->setErrorFromStatusAfterAddRecipientToList($status);

        // common error code for all errors (for 'security purpose')
        if ($status !== 0) {
            $status = self::RECIPIENT_ADD_VALIDATION_ERROR;
        }

        return $status;
    }

    /**
     *
     * Creates a new recipient filter with name $filterName and filter by $topicName
     *
     * @param string $filterName
     * @param string $topicId
     *
     * @return int 0 on error or if filter already exists, otherwise filterId
     */
    public function createTopicFilter($filterName, $topicId) {
        $filterId = 0;
        //@todo check for existing RecipientFilters
        try {
            // unfortunately filters needs to be initiated with a condition :(
            $filterId = $this->RecipientFilterWebservice()
                ->create($this->sessionId, $filterName, FALSE, $this->topicsAttributes[0], 'contains', array($topicId));
            $filterModificationId = $this->RecipientFilterWebservice()
                ->beginConditionModification($this->sessionId, $filterId);
            $this->RecipientFilterWebservice()
                ->clearConditions($this->sessionId, $filterModificationId);

            foreach ($this->topicsAttributes as $topicAttribute) {
                $this->RecipientFilterWebservice()
                    ->addOrCondition($this->sessionId, $filterModificationId, FALSE, $topicAttribute, 'contains', array($topicId));
            }
            $this->RecipientFilterWebservice()
                ->commitConditionModification($this->sessionId, $filterModificationId);

        } catch (SoapFault $exception) {
            $this->error = $exception->getMessage();
        }
        return $filterId;
    }

    /**
     * Method to set recipients attributes
     *
     * @param string $recipientMailAddress
     * @param string[] $attributes indexed by attribute key
     */
    public function setAttributes($recipientMailAddress, $attributes) {

        $attributeNames = array_keys($attributes);
        $attributeValues = array_values($attributes);

        $recipientID = $this->getRecipientId($recipientMailAddress);
        try {
            $this->RecipientWebservice()
                ->setAttributes($this->sessionId, $this->recipientListId, $recipientID, $attributeNames, $attributeValues);
        } catch (SoapFault $exception) {
            $this->error = $exception->getMessage();
        }
    }

    /**
     * Temporary helper method: returns recipients attributes
     *
     * @param string[] $attributeNames filtered by given attributeNames
     *
     * @return string[][] returns all if empty parameter $attributesNames
     */
    public function recipientGetAllByAttributes($attributeNames = array()) {
        if (!count($attributeNames)) {
            $attributeNames = $this->RecipientListWebservice()
                ->getAttributeNames($this->sessionId, $this->recipientListId, $this->language);
        }
        return $this->RecipientWebservice()
            ->getAll($this->sessionId, $this->recipientListId, $attributeNames);
    }

    /**
     * Returns true if mailAddress is in recipientList
     * @param string $mailAddress
     *
     * @return boolean
     */
    public function containsByMailaddress($mailAddress) {
        $recipientId = $this->getRecipientId($mailAddress);
        return $this->containsById($recipientId);
    }

    /**
     * Returns true if recipientId is in recipientList
     * @param string $recipientId
     *
     * @return boolean
     */
    public function containsById($recipientId) {
        return $this->RecipientWebservice()
            ->contains($this->sessionId, $this->recipientListId, $recipientId);
    }

    /**
     * Generates recipientId
     * @param $mailAddress
     * @return string
     */
    public static function getRecipientId($mailAddress) {
        return $mailAddress;
    }

    /**
     * Add a new recipient to Optivo Recipient list
     *
     * @param string $recipientId
     * @param string $emailAddress
     * @param array $attributeNames
     * @param array $attributeValues
     * @param array $topicNames
     * @return int
     */
    private function addNewRecipientToList($recipientId, $emailAddress, $attributeNames, $attributeValues, $topicNames) {

        // Add source url if the list supports this field
        $this->addSourceUrlToAttributes($attributeNames, $attributeValues);

        // prepare topic to add
        $recipientTopics = $this->prepareRecipientTopicsToAdd(array_fill(0, count($this->topicsAttributes), ''), $topicNames);
        // update recipient
        $attributeNames = array_merge($attributeNames, array_keys($recipientTopics));
        $attributeValues = array_merge($attributeValues, array_values($recipientTopics));

        try {
            $status = $this->RecipientWebservice()
                ->add2($this->sessionId, $this->recipientListId, $this->optinProcessId, $recipientId, $emailAddress, $attributeNames, $attributeValues);
        } catch (SoapFault $exception) {
            //@todo error handling
            $this->error = $exception->getMessage();
        }

        return $status;
    }

    /**
     * Add source url to attributes if the current recipient list supports this field
     *
     * @param array $attributeNames
     * @param array $attributeValues
     */
    private function addSourceUrlToAttributes(& $attributeNames, & $attributeValues) {
        // Get attribute names from current recipient list
        $recipientListAttributeNames = $this->RecipientListWebservice()->getAttributeNames($this->getSessionId(), $this->recipientListId, '');

        // Check if this recipient list has the field for subscribe url
        if (in_array(self::SUBSCRIBE_URL_LABEL, $recipientListAttributeNames)) {
            // Add source url to recipient
            $attributeNames[] = self::SUBSCRIBE_URL_ATTR;
            $attributeValues[] = GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL');
        }
    }

    /**
     * Method to add a topic to recipients attribute
     *
     * @param string $recipientMailaddress
     * @param array $topicNames
     *
     * @return bool | int
     */
    private function addRecipientTopic($recipientMailaddress, $topicNames) {

        $recipientId = $this->getRecipientId($recipientMailaddress);

        // get current attributes from recipient
        try {
            $recipientTopicValues = $this->RecipientWebservice()
                ->getAttributes($this->sessionId, $this->recipientListId, $recipientId, $this->topicsAttributes);
        } catch (SoapFault $exception) {
            $this->error = $exception->getMessage();
            return FALSE;
        }
        $topicNames = $this->filterExistingRecipientTopics($recipientTopicValues, $topicNames);

        // if no topics left after removing existing ones, nothing to be done here
        if (empty($topicNames)) {
            return TRUE;
        }
        // otherwise add them to recipient
        else {
            // prepare topic to add
            $recipientTopics = $this->prepareRecipientTopicsToAdd($recipientTopicValues, $topicNames);
            // update recipient
            $this->setAttributes($recipientMailaddress, $recipientTopics);
        }

        if ($this->error) {
            return self::RECIPIENT_UPDATE_ATTRIB_OVERFLOW;
        } else {
            return TRUE;
        }
    }

    /**
     *  Set error messages, watch out - some strange logic ahead (!)
     *
     * @param int $status
     */
    private function setErrorFromStatusAfterAddRecipientToList($status) {
        switch ($status) {
            case self::RECIPIENT_ADD_COMMON_ERROR :
                $this->error = 'Common error during recipient insertion.';
                break;
            case self::RECIPIENT_ADD_VALIDATION_ERROR :
                $this->error = 'Validation error during recipient insertion.';
                break;
            case self::RECIPIENT_ADD_ON_RESTRICLIST :
            case self::RECIPIENT_ADD_ON_CANCELLIST :
                $this->error = 'Recipient is on cancel list and could not be inserted.';
                break;
            case self::RECIPIENT_ADD_BOUNCE_LIMIT :
                $this->error = 'Recipient has reached bounce and could not be inserted.';
                break;
            case self::RECIPIENT_ADD_WAS_FILTERD :
                $this->error = 'Recipient was filtered and could not be inserted.';
                break;
            case self::RECIPIENT_ADD_ALREADY_INLIST :
                $this->error = 'Recipient is already in list and could not be inserted. It was updated instead.';
                break;
            default :
        }
    }

    /**
     * Helper method to get attributes field to put next topic
     * @param $recipientTopics
     * @param $topicName
     * @return mixed
     */
    private function getAttributeKey($recipientTopics, $topicName) {
        // find attribute field with space for new topic to append to
        $topicKey = key($recipientTopics);
        foreach ($recipientTopics as $key => $topicAtrributes) {
            if (strlen($topicAtrributes . $topicName . ';') > self::MAX_TOPIC_ATTRIBUTE_LENGTH) {
                continue;
            }
            else {
                $topicKey = $key;
                break;
            }
        }
        return $topicKey;
    }

    /**
     * Returns if OptivoConnection is valid (i.e. session created)
     * @return bool
     */
    private function isValid() {
        return (bool) $this->sessionId;
    }

    /**
     * Setter method for topicsAttributes
     */
    private function setTopicsAttributes() {
        // initialize topic attributes for filters and recipient properties
        foreach (range(1, 5) as $number) {
            $this->topicsAttributes[] = '' . self::TOPIC_ATTRIB_BASE_STR . '' . $number;
        }
    }

    /**
     * Filter topicNames by existing recipientTopics
     * @param array $recipientTopics
     * @param array $topicNames
     * @return array $topicNames filtered
     */
    private function filterExistingRecipientTopics($recipientTopics, $topicNames) {
        $flatRecipientTopics = implode(';', $recipientTopics);
        // find existing topics in recipient attributes
        foreach ($topicNames as $key => $topicName) {
            // if topic already in recipient attributes ...
            $isTopicInAttributes = preg_grep('/' . $topicName . '/', $recipientTopics);
            if ($isTopicInAttributes !== FALSE && count($isTopicInAttributes)) {
                // ... remove topic from attributes array
                unset($topicNames[$key]);
            }
        }

        return $topicNames;
    }

    /**
     * Helper method to prepare topic indexes and array for adding
     * @param $recipientTopicValues
     * @param $topicNames
     * @return array
     */
    private function prepareRecipientTopicsToAdd($recipientTopicValues, $topicNames) {
        $recipientTopics = array_combine($this->topicsAttributes, $recipientTopicValues);
        foreach ($topicNames as $key => $topicName) {
            // if got a valid topic key
            if ($topicKey = $this->getAttributeKey($recipientTopics, $topicName)) {
                $recipientTopics[$topicKey] .= $topicName . ';';
            }
            else {
                // otherwise return error
                $this->error = 'No space left in reciepients attributes to add topic: "' . $topicName . '""';
                break;
            }
        }
        return $recipientTopics;
    }
}
