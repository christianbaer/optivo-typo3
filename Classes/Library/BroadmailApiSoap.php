<?php

namespace Pixelpark\Optivo\Library;
use SoapClient;
use SoapFault;

/**
 * The following definitions are just type hints to support auto complete for nicer development.
 * The real work is done by the php SoapClient.
 */
abstract class AttachmentWebservice
{
  /**
   * @param string $sessionId
   * @param string $name
   * @param string $mimeType
   * @param string $filename
   * @param string $content
   *
   * @return int
   */
  abstract public function create($sessionId, $name, $mimeType, $filename, $content);

  /**
   * @param string $sessionId
   *
   * @return int[]
   */
  abstract public function getAllIds($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getColumnNames($sessionId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   *
   * @return string
   */
  abstract public function getContent($sessionId, $attachmentId);

  /**
   * @param string $sessionId
   *
   * @return int
   */
  abstract public function getCount($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[][]
   */
  abstract public function getDataSet($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getDataSetFlat($sessionId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   *
   * @return string
   */
  abstract public function getFilename($sessionId, $attachmentId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   *
   * @return string
   */
  abstract public function getMimeType($sessionId, $attachmentId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   *
   * @return string
   */
  abstract public function getName($sessionId, $attachmentId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   *
   * @return int
   */
  abstract public function getSize($sessionId, $attachmentId);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   * @param string $content
   */
  abstract public function setContent($sessionId, $attachmentId, $content);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   * @param string $filename
   */
  abstract public function setFilename($sessionId, $attachmentId, $filename);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   * @param string $mimeType
   */
  abstract public function setMimeType($sessionId, $attachmentId, $mimeType);

  /**
   * @param string $sessionId
   * @param int $attachmentId
   * @param string $name
   */
  abstract public function setName($sessionId, $attachmentId, $name);

}

abstract class BasicReportingWebservice
{
  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param boolean $separateByLink
   * @param string[] $linkUrls
   *
   * @return string[][]
   */
  abstract public function getClicks($sessionId, $mailingIds, $asPercentage, $separateByLink, $linkUrls);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $separateByDomains
   * @param boolean $asPercentage
   * @param string[] $domains
   *
   * @return string[][]
   */
  abstract public function getClicksByDomain($sessionId, $mailingIds, $separateByDomains, $asPercentage, $domains);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param string $startDate
   * @param string $endDate
   * @param string $interval
   *
   * @return string[][]
   */
  abstract public function getClicksByTime($sessionId, $mailingIds, $asPercentage, $startDate, $endDate, $interval);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   *
   * @return string[][]
   */
  abstract public function getNumberOfEmails($sessionId, $mailingIds);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $separateByDomains
   * @param boolean $asPercentage
   * @param string[] $domains
   *
   * @return string[][]
   */
  abstract public function getNumberOfEmailsByDomain($sessionId, $mailingIds, $separateByDomains, $asPercentage, $domains);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   *
   * @return string[][]
   */
  abstract public function getOpens($sessionId, $mailingIds, $asPercentage);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $separateByDomains
   * @param boolean $asPercentage
   * @param string[] $domains
   *
   * @return string[][]
   */
  abstract public function getOpensByDomain($sessionId, $mailingIds, $separateByDomains, $asPercentage, $domains);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param string $startDate
   * @param string $endDate
   * @param string $interval
   *
   * @return string[][]
   */
  abstract public function getOpensByTime($sessionId, $mailingIds, $asPercentage, $startDate, $endDate, $interval);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param boolean $separateByResponseType
   * @param string[] $categories
   *
   * @return string[][]
   */
  abstract public function getResponses($sessionId, $mailingIds, $asPercentage, $separateByResponseType, $categories);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $separateByDomains
   * @param boolean $asPercentage
   * @param string[] $domains
   *
   * @return string[][]
   */
  abstract public function getResponsesByDomain($sessionId, $mailingIds, $separateByDomains, $asPercentage, $domains);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param string $startDate
   * @param string $endDate
   * @param string $interval
   *
   * @return string[][]
   */
  abstract public function getResponsesByTime($sessionId, $mailingIds, $asPercentage, $startDate, $endDate, $interval);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   *
   * @return string[][]
   */
  abstract public function getUnsubscribes($sessionId, $mailingIds, $asPercentage);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $separateByDomains
   * @param boolean $asPercentage
   * @param string[] $domains
   *
   * @return string[][]
   */
  abstract public function getUnsubscribesByDomain($sessionId, $mailingIds, $separateByDomains, $asPercentage, $domains);

  /**
   * @param string $sessionId
   * @param int[] $mailingIds
   * @param boolean $asPercentage
   * @param string $startDate
   * @param string $endDate
   * @param string $interval
   *
   * @return string[][]
   */
  abstract public function getUnsubscribesByTime($sessionId, $mailingIds, $asPercentage, $startDate, $endDate, $interval);

}

abstract class BlacklistWebservice
{
  /**
   * @param string $sessionId
   * @param string $entry
   * @param string $reason
   */
  abstract public function add($sessionId, $entry, $reason);

  /**
   * @param string $sessionId
   * @param string[] $entries
   * @param string[] $reasons
   */
  abstract public function addAll($sessionId, $entries, $reasons);

  /**
   * @param string $sessionId
   * @param string[] $emailAddresses
   *
   * @return boolean[]
   */
  abstract public function areBlacklisted($sessionId, $emailAddresses);

  /**
   * @param string $sessionId
   * @param string $entry
   *
   * @return boolean
   */
  abstract public function contains($sessionId, $entry);

  /**
   * @param string $sessionId
   * @param string[] $entries
   *
   * @return boolean[]
   */
  abstract public function containsAll($sessionId, $entries);

  /**
   * @param string $sessionId
   * @param int $pageStart
   * @param int $pageSize
   *
   * @return string[][]
   */
  abstract public function getAllAdvanced($sessionId, $pageStart, $pageSize);

  /**
   * @param string $sessionId
   * @param int $pageStart
   * @param int $pageSize
   *
   * @return string[]
   */
  abstract public function getAllAdvancedFlat($sessionId, $pageStart, $pageSize);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getAllEntries($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getColumnNames($sessionId);

  /**
   * @param string $sessionId
   *
   * @return int
   */
  abstract public function getCount($sessionId);

  /**
   * @param string $sessionId
   * @param string $entry
   *
   * @return string
   */
  abstract public function getCreated($sessionId, $entry);

  /**
   * @param string $sessionId
   *
   * @return string[][]
   */
  abstract public function getDataSet($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getDataSetFlat($sessionId);

  /**
   * @param string $sessionId
   * @param string $emailAddress
   *
   * @return string
   */
  abstract public function getFirstMatchingEntry($sessionId, $emailAddress);

  /**
   * @param string $sessionId
   * @param string $entry
   *
   * @return string
   */
  abstract public function getReason($sessionId, $entry);

  /**
   * @param string $sessionId
   * @param string $emailAddress
   *
   * @return boolean
   */
  abstract public function isBlacklisted($sessionId, $emailAddress);

  /**
   * @param string $sessionId
   * @param string $entry
   */
  abstract public function remove($sessionId, $entry);

  /**
   * @param string $sessionId
   * @param string[] $entries
   */
  abstract public function removeAll($sessionId, $entries);

}

abstract class ClosedLoopWebservice
{
  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getClicks($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   *
   * @return int
   */
  abstract public function getCurrentTime($sessionId);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getLinks($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $waveId
   *
   * @return int
   */
  abstract public function getMailingIdByWaveId($sessionId, $waveId);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getMailingUnsubscribes($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getMailings($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getOpens($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getOutBounces($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getRecipients($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getResponses($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $since
   * @param int $until
   * @param int $startRow
   * @param int $numberOfRows
   *
   * @return string[][]
   */
  abstract public function getUnsubscribes($sessionId, $since, $until, $startRow, $numberOfRows);

  /**
   * @param string $sessionId
   * @param int $waveId
   */
  abstract public function importFinishedAndScheduleMailing($sessionId, $waveId);

  /**
   * @param string $sessionId
   * @param int $waveId
   * @param string[] $recipientFieldNames
   * @param string[][] $recipientFieldValues
   */
  abstract public function importRecipients($sessionId, $waveId, $recipientFieldNames, $recipientFieldValues);

  /**
   * @param string $sessionId
   * @param int $waveId
   * @param string[] $recipientFieldNames
   * @param string[] $flatRecipientFieldValues
   */
  abstract public function importRecipientsFlat($sessionId, $waveId, $recipientFieldNames, $flatRecipientFieldValues);

  /**
   * @param string $sessionId
   * @param int $templateMailingId
   *
   * @return int
   */
  abstract public function prepareNewWave($sessionId, $templateMailingId);

}

abstract class FolderWebservice
{
  /**
   * @param string $sessionId
   * @param int $objectId
   * @param int $folderId
   * @param string $folderType
   */
  abstract public function assignFolder($sessionId, $objectId, $folderId, $folderType);

  /**
   * @param string $sessionId
   * @param string $folderName
   * @param int $parentFolderId
   * @param string $folderType
   *
   * @return int
   */
  abstract public function createFolder($sessionId, $folderName, $parentFolderId, $folderType);

  /**
   * @param string $sessionId
   * @param int $objectId
   *
   * @return int
   */
  abstract public function getAssignedFolder($sessionId, $objectId);

  /**
   * @param string $sessionId
   * @param int $folderId
   *
   * @return int[]
   */
  abstract public function getChildren($sessionId, $folderId);

  /**
   * @param string $sessionId
   * @param int $folderId
   *
   * @return string
   */
  abstract public function getFolderName($sessionId, $folderId);

  /**
   * @param string $sessionId
   * @param int $folderId
   *
   * @return int
   */
  abstract public function getParent($sessionId, $folderId);

  /**
   * @param string $sessionId
   * @param string $folderType
   *
   * @return int[]
   */
  abstract public function getRootFolders($sessionId, $folderType);

  /**
   * @param string $sessionId
   * @param int $folderId
   * @param int $newParentFolderId
   */
  abstract public function moveFolder($sessionId, $folderId, $newParentFolderId);

  /**
   * @param string $sessionId
   * @param int $folderId
   *
   * @return boolean
   */
  abstract public function removeFolder($sessionId, $folderId);

  /**
   * @param string $sessionId
   * @param int $folderId
   * @param string $folderName
   */
  abstract public function renameFolder($sessionId, $folderId, $folderName);

}

abstract class MailIdWebservice
{
  /**
   * @param string $sessionId
   * @param string $mailId
   *
   * @return int
   */
  abstract public function getMailingId($sessionId, $mailId);

  /**
   * @param string $sessionId
   * @param string $mailId
   *
   * @return int
   */
  abstract public function getMandatorId($sessionId, $mailId);

  /**
   * @param string $sessionId
   * @param string $mailId
   *
   * @return string
   */
  abstract public function getRecipientId($sessionId, $mailId);

  /**
   * @param string $sessionId
   * @param string $mailId
   *
   * @return int
   */
  abstract public function getRecipientListId($sessionId, $mailId);

}

abstract class MailingReportingWebservice
{
  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param boolean $unique
   *
   * @return int
   */
  abstract public function getClickCount($sessionId, $mailingId, $unique);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string[] $urls
   * @param boolean $unique
   *
   * @return int[]
   */
  abstract public function getClickCountByUrl($sessionId, $mailingId, $urls, $unique);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getFailedRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string[]
   */
  abstract public function getLinkNames($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string[]
   */
  abstract public function getLinkUrls($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param boolean $unique
   *
   * @return int
   */
  abstract public function getOpenCount($sessionId, $mailingId, $unique);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getOverallRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $category
   *
   * @return int
   */
  abstract public function getResponseCount($sessionId, $mailingId, $category);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getSentRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getUnsubscribeCount($sessionId, $mailingId);

}

abstract class MailingWebservice
{
  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function cancel($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function copy($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param string $mailingType
   * @param string $name
   * @param string $mimeType
   * @param int[] $recipientListIds
   * @param string $senderEmailPrefix
   * @param string $senderName
   * @param string $charset
   *
   * @return int
   */
  abstract public function create($sessionId, $mailingType, $name, $mimeType, $recipientListIds, $senderEmailPrefix, $senderName, $charset);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $content
   * @param string $mimeType
   *
   * @return string
   */
  abstract public function decodeTrackingLinks($sessionId, $mailingId, $content, $mimeType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $content
   * @param string $mimeType
   *
   * @return string
   */
  abstract public function encodeTrackingLinks($sessionId, $mailingId, $content, $mimeType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getAttachmentIds($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getCharset($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param string $mailingType
   *
   * @return string[]
   */
  abstract public function getColumnNames($sessionId, $mailingType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   *
   * @return string
   */
  abstract public function getContent($sessionId, $mailingId, $mimeType);

  /**
   * @param string $sessionId
   * @param string $mailingType
   *
   * @return int
   */
  abstract public function getCount($sessionId, $mailingType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getCreatedDate($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param string $mailingType
   *
   * @return string[][]
   */
  abstract public function getDataSet($sessionId, $mailingType);

  /**
   * @param string $sessionId
   * @param string $mailingType
   *
   * @return string[]
   */
  abstract public function getDataSetFlat($sessionId, $mailingType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getDescription($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getFailedRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getFromEmailPrefix($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getFromName($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $name
   *
   * @return string[]
   */
  abstract public function getHeader($sessionId, $mailingId, $name);

  /**
   * @param string $sessionId
   * @param string $mailingType
   *
   * @return int[]
   */
  abstract public function getIds($sessionId, $mailingType);

  /**
   * @param string $sessionId
   * @param string $mailingType
   * @param string $mailingStatus
   *
   * @return int[]
   */
  abstract public function getIdsInStatus($sessionId, $mailingType, $mailingStatus);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getMaxMailsPerHour($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return float
   */
  abstract public function getMaxRecipients($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getMimeType($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getName($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getOverallRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getPredictedRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $name
   *
   * @return string
   */
  abstract public function getProperty($sessionId, $mailingId, $name);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getRecipientFilterId($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getRecipientFilterIds($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getRecipientListIds($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getScheduleDate($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getSendingFinishedDate($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getSendingStartedDate($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return string[]
   */
  abstract public function getSendingStatus($sessionId, $mailingId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getSentRecipientCount($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getStatus($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return string
   */
  abstract public function getSubject($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   *
   * @return boolean
   */
  abstract public function isClickTrackingEnabled($sessionId, $mailingId, $mimeType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return boolean
   */
  abstract public function isDefaultRecipientFilterEnabled($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return boolean
   */
  abstract public function isMaxRecipientsPercentage($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return boolean
   */
  abstract public function isMaxRecipientsRandomOrder($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return boolean
   */
  abstract public function isOpenTrackingEnabled($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return boolean
   */
  abstract public function isVerpEnabled($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function pause($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function remove($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function restart($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function resume($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return int
   */
  abstract public function sendMail($sessionId, $mailingId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   * @param string[] $recipientIds
   *
   * @return int[]
   */
  abstract public function sendMails($sessionId, $mailingId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return int
   */
  abstract public function sendTestMail($sessionId, $mailingId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return int
   */
  abstract public function sendTestMail2($sessionId, $mailingId, $mimeType, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   *
   * @return int[]
   */
  abstract public function sendTestMailToAll($sessionId, $mailingId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientListId
   * @param string[] $recipientIds
   *
   * @return int[]
   */
  abstract public function sendTestMails($sessionId, $mailingId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int[] $attachmentIds
   */
  abstract public function setAttachmentIds($sessionId, $mailingId, $attachmentIds);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $charset
   */
  abstract public function setCharset($sessionId, $mailingId, $charset);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   * @param boolean $enabled
   */
  abstract public function setClickTrackingEnabled($sessionId, $mailingId, $mimeType, $enabled);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   * @param string $content
   */
  abstract public function setContent($sessionId, $mailingId, $mimeType, $content);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param boolean $enabled
   */
  abstract public function setDefaultRecipientFilterEnabled($sessionId, $mailingId, $enabled);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $description
   */
  abstract public function setDescription($sessionId, $mailingId, $description);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $senderEmailPrefix
   * @param string $senderName
   */
  abstract public function setFrom($sessionId, $mailingId, $senderEmailPrefix, $senderName);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $name
   * @param string $value
   */
  abstract public function setHeader($sessionId, $mailingId, $name, $value);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $maxMailsPerHour
   */
  abstract public function setMaxMailsPerHour($sessionId, $mailingId, $maxMailsPerHour);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $maxRecipients
   * @param boolean $randomOrder
   */
  abstract public function setMaxRecipients($sessionId, $mailingId, $maxRecipients, $randomOrder);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param float $maxRecipientsPercentage
   * @param boolean $randomOrder
   */
  abstract public function setMaxRecipientsPercentage($sessionId, $mailingId, $maxRecipientsPercentage, $randomOrder);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   */
  abstract public function setMimeType($sessionId, $mailingId, $mimeType);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $name
   */
  abstract public function setName($sessionId, $mailingId, $name);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param boolean $enabled
   */
  abstract public function setOpenTrackingEnabled($sessionId, $mailingId, $enabled);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $name
   * @param string $value
   */
  abstract public function setProperty($sessionId, $mailingId, $name, $value);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $recipientFilterId
   */
  abstract public function setRecipientFilterId($sessionId, $mailingId, $recipientFilterId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int[] $recipientFilterIds
   */
  abstract public function setRecipientFilterIds($sessionId, $mailingId, $recipientFilterIds);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int[] $recipientListIds
   */
  abstract public function setRecipientListIds($sessionId, $mailingId, $recipientListIds);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $replyToAddress
   */
  abstract public function setReplyToAddress($sessionId, $mailingId, $replyToAddress);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $replyToName
   */
  abstract public function setReplyToName($sessionId, $mailingId, $replyToName);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $subject
   */
  abstract public function setSubject($sessionId, $mailingId, $subject);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param boolean $verpEnabled
   */
  abstract public function setVerpEnabled($sessionId, $mailingId, $verpEnabled);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $startDate
   */
  abstract public function start($sessionId, $mailingId, $startDate);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $mimeType
   * @param string $content
   *
   * @return string
   */
  abstract public function validateContent($sessionId, $mailingId, $mimeType, $content);

}

abstract class OptinProcessWebservice
{
  /**
   * @param string $sessionId
   * @param string $name
   * @param string $description
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function createConfirmedOptinProcess($sessionId, $name, $description, $mailingId);

  /**
   * @param string $sessionId
   * @param string $name
   * @param string $description
   * @param int $mailingId
   * @param string $confirmationUrl
   *
   * @return int
   */
  abstract public function createDoubleOptinProcess($sessionId, $name, $description, $mailingId, $confirmationUrl);

  /**
   * @param string $sessionId
   * @param string $name
   * @param string $description
   *
   * @return int
   */
  abstract public function createSingleOptinProcess($sessionId, $name, $description);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   *
   * @return int
   */
  abstract public function getConfirmationMailingId($sessionId, $optinProcessId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   *
   * @return string
   */
  abstract public function getConfirmationUrl($sessionId, $optinProcessId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   *
   * @return string
   */
  abstract public function getDescription($sessionId, $optinProcessId);

  /**
   * @param string $sessionId
   *
   * @return int[]
   */
  abstract public function getIds($sessionId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   *
   * @return string
   */
  abstract public function getName($sessionId, $optinProcessId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   *
   * @return string
   */
  abstract public function getType($sessionId, $optinProcessId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   * @param int $mailingId
   */
  abstract public function setConfirmationMailingId($sessionId, $optinProcessId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   * @param string $url
   */
  abstract public function setConfirmationUrl($sessionId, $optinProcessId, $url);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   * @param string $description
   */
  abstract public function setDescription($sessionId, $optinProcessId, $description);

  /**
   * @param string $sessionId
   * @param int $optinProcessId
   * @param string $name
   */
  abstract public function setName($sessionId, $optinProcessId, $name);

}

abstract class RecipientFilterWebservice
{
  /**
   * @param string $sessionId
   * @param string $modificationId
   * @param boolean $negateCondition
   * @param string $attributeName
   * @param string $operation
   * @param string[] $operationParameters
   */
  abstract public function addAndCondition($sessionId, $modificationId, $negateCondition, $attributeName, $operation, $operationParameters);

  /**
   * @param string $sessionId
   * @param string $modificationId
   * @param boolean $negateCondition
   * @param string $attributeName
   * @param string $operation
   * @param string[] $operationParameters
   */
  abstract public function addOrCondition($sessionId, $modificationId, $negateCondition, $attributeName, $operation, $operationParameters);

  /**
   * @param string $sessionId
   * @param int $filterId
   *
   * @return string
   */
  abstract public function beginConditionModification($sessionId, $filterId);

  /**
   * @param string $sessionId
   * @param string $modificationId
   */
  abstract public function cancelConditionModification($sessionId, $modificationId);

  /**
   * @param string $sessionId
   * @param string $modificationId
   */
  abstract public function clearConditions($sessionId, $modificationId);

  /**
   * @param string $sessionId
   * @param string $modificationId
   */
  abstract public function commitConditionModification($sessionId, $modificationId);

  /**
   * @param string $sessionId
   * @param string $filterName
   * @param boolean $negateCondition
   * @param string $attributeName
   * @param string $operation
   * @param string[] $operationParameters
   *
   * @return int
   */
  abstract public function create($sessionId, $filterName, $negateCondition, $attributeName, $operation, $operationParameters);

  /**
   * @param string $sessionId
   * @param boolean $negateCondition
   * @param string $attributeName
   * @param string $operation
   * @param string[] $operationParameters
   *
   * @return int
   */
  abstract public function createTemporary($sessionId, $negateCondition, $attributeName, $operation, $operationParameters);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getColumnNames($sessionId);

  /**
   * @param string $sessionId
   *
   * @return int
   */
  abstract public function getCount($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[][]
   */
  abstract public function getDataSet($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getDataSetFlat($sessionId);

  /**
   * @param string $sessionId
   * @param int $filterId
   *
   * @return string
   */
  abstract public function getDescription($sessionId, $filterId);

  /**
   * @param string $sessionId
   *
   * @return int[]
   */
  abstract public function getIds($sessionId);

  /**
   * @param string $sessionId
   * @param int $filterId
   *
   * @return string
   */
  abstract public function getName($sessionId, $filterId);

  /**
   * @param string $sessionId
   * @param int $filterId
   *
   * @return boolean
   */
  abstract public function isInUse($sessionId, $filterId);

  /**
   * @param string $sessionId
   * @param int $filterId
   *
   * @return boolean
   */
  abstract public function isTemporary($sessionId, $filterId);

  /**
   * @param string $sessionId
   * @param int $filterId
   * @param string $description
   */
  abstract public function setDescription($sessionId, $filterId, $description);

  /**
   * @param string $sessionId
   * @param int $filterId
   * @param string $name
   */
  abstract public function setName($sessionId, $filterId, $name);

}

abstract class RecipientListWebservice
{
  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return int
   */
  abstract public function copy($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   *
   * @return int[]
   */
  abstract public function getAllIds($sessionId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $locale
   *
   * @return string[]
   */
  abstract public function getAttributeNames($sessionId, $recipientListId, $locale);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getColumnNames($sessionId);

  /**
   * @param string $sessionId
   * @param boolean $includeTestLists
   *
   * @return int
   */
  abstract public function getCount($sessionId, $includeTestLists);

  /**
   * @param string $sessionId
   *
   * @return string[][]
   */
  abstract public function getDataSet($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string[]
   */
  abstract public function getDataSetFlat($sessionId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return string
   */
  abstract public function getDescription($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return string
   */
  abstract public function getName($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return boolean
   */
  abstract public function isTestRecipientList($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $description
   */
  abstract public function setDescription($sessionId, $recipientListId, $description);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $name
   */
  abstract public function setName($sessionId, $recipientListId, $name);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param boolean $testList
   */
  abstract public function setTestRecipientList($sessionId, $recipientListId, $testList);

}

abstract class RecipientWebservice
{
  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string $recipientId
   * @param string $address
   * @param string[] $attributeNames
   * @param string[] $attributeValues
   *
   * @return boolean
   */
  abstract public function add($sessionId, $recipientListId, $optinProcessId, $recipientId, $address, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string $recipientId
   * @param string $address
   * @param string[] $attributeNames
   * @param string[] $attributeValues
   *
   * @return int
   */
  abstract public function add2($sessionId, $recipientListId, $optinProcessId, $recipientId, $address, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string[] $recipientIds
   * @param string[] $addresses
   * @param string[] $attributeNames
   * @param string[][] $attributeValues
   *
   * @return boolean[]
   */
  abstract public function addAll($sessionId, $recipientListId, $optinProcessId, $recipientIds, $addresses, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string[] $recipientIds
   * @param string[] $addresses
   * @param string[] $attributeNames
   * @param string[][] $attributeValues
   *
   * @return int[]
   */
  abstract public function addAll2($sessionId, $recipientListId, $optinProcessId, $recipientIds, $addresses, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string[] $recipientIds
   * @param string[] $addresses
   * @param string[] $attributeNames
   * @param string[] $flatAttributeValues
   *
   * @return int[]
   */
  abstract public function addAll2Flat($sessionId, $recipientListId, $optinProcessId, $recipientIds, $addresses, $attributeNames, $flatAttributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string[] $recipientIds
   * @param string[] $emailAddresses
   * @param string[] $attributeNames
   * @param string[][] $attributeValues
   *
   * @return int[]
   */
  abstract public function addAll3($sessionId, $recipientListId, $optinProcessId, $recipientIds, $emailAddresses, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $optinProcessId
   * @param string[] $recipientIds
   * @param string[] $emailAddresses
   * @param string[] $attributeNames
   * @param string[] $flatAttributeValues
   *
   * @return int[]
   */
  abstract public function addAll3Flat($sessionId, $recipientListId, $optinProcessId, $recipientIds, $emailAddresses, $attributeNames, $flatAttributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   * @param string $newRecipientId
   */
  abstract public function changeRecipientId($sessionId, $recipientListId, $recipientId, $newRecipientId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   * @param string $newRecipientId
   */
  abstract public function changeRecipientId2($sessionId, $recipientId, $newRecipientId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   */
  abstract public function clear($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function contains($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int[] $recipientListIds
   * @param string $recipientId
   *
   * @return boolean[]
   */
  abstract public function containsMultiple($sessionId, $recipientListIds, $recipientId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function containsValid($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int[] $recipientListIds
   * @param string $recipientId
   *
   * @return boolean[]
   */
  abstract public function containsValidMultiple($sessionId, $recipientListIds, $recipientId);

  /**
   * @param string $sessionId
   * @param string $recipientKey
   */
  abstract public function createTrackingOptOut($sessionId, $recipientKey);

  /**
   * @param string $sessionId
   * @param string $recipientKey
   */
  abstract public function deleteTrackingOptOut($sessionId, $recipientKey);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $attributeNames
   *
   * @return string[][]
   */
  abstract public function getAll($sessionId, $recipientListId, $attributeNames);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $attributeNames
   * @param int $recipientFilterId
   * @param string $orderByAttribute
   * @param boolean $ascendingSortOrder
   * @param int $pageStart
   * @param int $pageSize
   *
   * @return string[][]
   */
  abstract public function getAllAdvanced($sessionId, $recipientListId, $attributeNames, $recipientFilterId, $orderByAttribute, $ascendingSortOrder, $pageStart, $pageSize);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $attributeNames
   * @param int $recipientFilterId
   * @param string $orderByAttribute
   * @param boolean $ascendingSortOrder
   * @param int $pageStart
   * @param int $pageSize
   *
   * @return string[]
   */
  abstract public function getAllAdvancedFlat($sessionId, $recipientListId, $attributeNames, $recipientFilterId, $orderByAttribute, $ascendingSortOrder, $pageStart, $pageSize);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $attributeNames
   *
   * @return string[]
   */
  abstract public function getAllFlat($sessionId, $recipientListId, $attributeNames);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   * @param string[] $attributeNames
   *
   * @return string[]
   */
  abstract public function getAttributes($sessionId, $recipientListId, $recipientId, $attributeNames);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $recipientFilterId
   *
   * @return int
   */
  abstract public function getCount($sessionId, $recipientListId, $recipientFilterId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $recipientFilterId
   * @param string $attributeName
   * @param int $threshold
   *
   * @return string[]
   */
  abstract public function getDistinctValues($sessionId, $recipientListId, $recipientFilterId, $attributeName, $threshold);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $recipientFilterId
   * @param string $attributeName
   * @param int $threshold
   *
   * @return string[][]
   */
  abstract public function getDistinctValuesCount($sessionId, $recipientListId, $recipientFilterId, $attributeName, $threshold);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $recipientFilterId
   * @param string $attributeName
   * @param int $threshold
   *
   * @return string[]
   */
  abstract public function getDistinctValuesCountFlat($sessionId, $recipientListId, $recipientFilterId, $attributeName, $threshold);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   */
  abstract public function remove($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   *
   * @return boolean[]
   */
  abstract public function removeAll($sessionId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return int
   */
  abstract public function removeBlacklisted($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return int
   */
  abstract public function removeBounced($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function removeNotOptined($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   *
   * @return int
   */
  abstract public function removeUnsubscribed($sessionId, $recipientListId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   * @param string[] $attributeNames
   * @param string[] $attributeValues
   */
  abstract public function setAttributes($sessionId, $recipientListId, $recipientId, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param int $recipientFilterId
   * @param string[] $attributeNames
   * @param string[] $attributeValues
   * @param string $mode
   */
  abstract public function setAttributesByFilter($sessionId, $recipientListId, $recipientFilterId, $attributeNames, $attributeValues, $mode);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   * @param string[][] $attributeNames
   * @param string[][] $attributeValues
   */
  abstract public function setAttributesInBatch($sessionId, $recipientListId, $recipientIds, $attributeNames, $attributeValues);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   * @param string[] $attributeNames
   * @param string[] $flatAttributeValues
   */
  abstract public function setAttributesInBatchFlat($sessionId, $recipientListId, $recipientIds, $attributeNames, $flatAttributeValues);

}

abstract class ResponseWebservice
{
  /**
   * @param string $sessionId
   * @param string $category
   * @param int $recipientListId
   * @param string[] $recipientIds
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getAllRecipientResponseCounts($sessionId, $category, $recipientListId, $recipientIds, $mailingId);

  /**
   * @param string $sessionId
   * @param string $category
   * @param string[] $recipientIds
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getAllRecipientResponseCounts2($sessionId, $category, $recipientIds, $mailingId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   * @param string $category
   *
   * @return int
   */
  abstract public function getBounceCounter($sessionId, $recipientId, $category);

  /**
   * @param string $sessionId
   * @param string $category
   *
   * @return int
   */
  abstract public function getBounceCounterThreshold($sessionId, $category);

  /**
   * @param string $sessionId
   * @param string $category
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getMailingResponseCount($sessionId, $category, $mailingId);

  /**
   * @param string $sessionId
   * @param string $category
   * @param int $recipientListId
   * @param string $recipientId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getRecipientResponseCount($sessionId, $category, $recipientListId, $recipientId, $mailingId);

  /**
   * @param string $sessionId
   * @param string $category
   * @param string $recipientId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getRecipientResponseCount2($sessionId, $category, $recipientId, $mailingId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function isBounceCounterThresholdExeeded($sessionId, $recipientId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   */
  abstract public function resetBounceCounter($sessionId, $recipientId);

}

abstract class SessionWebservice
{
  /**
   * @param string $sessionId
   *
   * @return string
   */
  abstract public function getLocale($sessionId);

  /**
   * @param string $sessionId
   *
   * @return string
   */
  abstract public function getMediaType($sessionId);

  /**
   * @param int $mandatorId
   * @param string $username
   * @param string $password
   *
   * @return string
   */
  abstract public function login($mandatorId, $username, $password);

  /**
   * @param string $sessionId
   */
  abstract public function logout($sessionId);

  /**
   * @param string $sessionId
   * @param string $locale
   */
  abstract public function setLocale($sessionId, $locale);

  /**
   * @param string $sessionId
   * @param string $mediaType
   */
  abstract public function setMediaType($sessionId, $mediaType);

}

abstract class SplitMailingWebservice
{
  /**
   * @param string $sessionId
   * @param int $masterId
   * @param int $childId
   */
  abstract public function addSplit($sessionId, $masterId, $childId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int[]
   */
  abstract public function getSplitChildIds($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   *
   * @return int
   */
  abstract public function getSplitMasterId($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   */
  abstract public function removeSplit($sessionId, $mailingId);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param string $criterion
   */
  abstract public function setFinalSplitSelectionCriterion($sessionId, $mailingId, $criterion);

  /**
   * @param string $sessionId
   * @param int $mailingId
   * @param int $days
   * @param int $hours
   * @param int $minutes
   */
  abstract public function setMasterStartDelay($sessionId, $mailingId, $days, $hours, $minutes);

}

abstract class UnsubscribeWebservice
{
  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   */
  abstract public function add($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   */
  abstract public function addAll($sessionId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param string $mailId
   */
  abstract public function addByMailId($sessionId, $mailId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function contains($sessionId, $recipientId);

  /**
   * @param string $sessionId
   * @param string[] $recipientIds
   *
   * @return boolean[]
   */
  abstract public function containsAll($sessionId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   *
   * @return boolean[]
   */
  abstract public function containsAllByRecipientList($sessionId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   *
   * @return boolean
   */
  abstract public function containsByRecipientList($sessionId, $recipientListId, $recipientId);

  /**
   * @param string $sessionId
   *
   * @return int
   */
  abstract public function getCount($sessionId);

  /**
   * @param string $sessionId
   * @param string $recipientId
   */
  abstract public function remove($sessionId, $recipientId);

  /**
   * @param string $sessionId
   * @param string[] $recipientIds
   */
  abstract public function removeAll($sessionId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string[] $recipientIds
   */
  abstract public function removeAllByRecipientList($sessionId, $recipientListId, $recipientIds);

  /**
   * @param string $sessionId
   * @param int $recipientListId
   * @param string $recipientId
   */
  abstract public function removeByRecipientList($sessionId, $recipientListId, $recipientId);

}

class BroadmailApiSoap
{
  /**
   * @return AttachmentWebservice
   */
  public static function AttachmentWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcAttachment?wsdl");
  }

  /**
   * @return BasicReportingWebservice
   */
  public static function BasicReportingWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcBasicReporting?wsdl");
  }

  /**
   * @return BlacklistWebservice
   */
  public static function BlacklistWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcBlacklist?wsdl");
  }

  /**
   * @return ClosedLoopWebservice
   */
  public static function ClosedLoopWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcClosedLoop?wsdl");
  }

  /**
   * @return FolderWebservice
   */
  public static function FolderWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcFolder?wsdl");
  }

  /**
   * @return MailIdWebservice
   */
  public static function MailIdWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcMailId?wsdl");
  }

  /**
   * @return MailingReportingWebservice
   */
  public static function MailingReportingWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcMailingReporting?wsdl");
  }

  /**
   * @return MailingWebservice
   */
  public static function MailingWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcMailing?wsdl");
  }

  /**
   * @return OptinProcessWebservice
   */
  public static function OptinProcessWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcOptinProcess?wsdl");
  }

  /**
   * @return RecipientFilterWebservice
   */
  public static function RecipientFilterWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcRecipientFilter?wsdl");
  }

  /**
   * @return RecipientListWebservice
   */
  public static function RecipientListWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcRecipientList?wsdl");
  }

  /**
   * @return RecipientWebservice
   */
  public static function RecipientWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcRecipient?wsdl");
  }

  /**
   * @return ResponseWebservice
   */
  public static function ResponseWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcResponse?wsdl");
  }

  /**
   * @return SessionWebservice
   */
  public static function SessionWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcSession?wsdl");
  }

  /**
   * @return SplitMailingWebservice
   */
  public static function SplitMailingWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcSplitMailing?wsdl");
  }

  /**
   * @return UnsubscribeWebservice
   */
  public static function UnsubscribeWebservice()
  {
    return new SoapClient("https://api.broadmail.de/soap11/RpcUnsubscribe?wsdl");
  }

}
