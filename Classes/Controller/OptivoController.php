<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Andreas Pinto-Koehler
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Pixelpark\Optivo\Controller;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use Pixelpark\Optivo\Service\OptivoConnection;

class OptivoController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

    public function resultOptivoAction() {
        $output = '';
        $params = (int) GeneralUtility::_GET('recipientStatus');
        switch ($params) {
            case OptivoConnection::RECIPIENT_ADD_SUCCESS :
                $output = 'Wir haben Dir eine E-Mail geschickt. Bitte schau in Dein Postfach für den Bestätigungslink.<br>Solltest Du keine Email bekommen haben, dann sieh bitte auch im Spam-Ordner nach oder schreib uns: dummy@domain.de';
                break;
            case OptivoConnection::RECIPIENT_ADD_VALIDATION_ERROR :
                $output = 'Bitte überprüfe Deine Eingabe.';
                break;
            case OptivoConnection::RECIPIENT_ADD_ON_CANCELLIST :
                $output = 'Deine E-Mailadresse ist auf der Sperrliste. Bitte kontaktiere uns über dummy@domain.de.';
                break;
            case OptivoConnection::RECIPIENT_ADD_BOUNCE_LIMIT :
                $output = 'Deine E-Mail-Adresse kann derzeit keine Mails empfangen. Bitte kontaktiere uns über dummy@domain.de.';
                break;
            default :
            case OptivoConnection::RECIPIENT_ADD_ALREADY_INLIST :
            case OptivoConnection::RECIPIENT_ADD_WAS_FILTERD :
            case OptivoConnection::RECIPIENT_ADD_COMMON_ERROR :
                $output = 'Ein allgemeiner Fehler ist aufgetreten. Bitte lade die Seite neu und gib Deine Daten nochmals ein. Sollte das Problem weiterhin bestehen, kontaktiere uns Bitte über dummy@domain.de.';
                break;
        }
        return $output;
    }
}
