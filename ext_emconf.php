<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "Pixelpark.Optivo".
 *
 * Auto generated 01-10-2015 20:07
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Optivo',
    'description' => 'Integration Optivo SOAP-API',
    'category' => 'misc',
    'shy' => 0,
    'version' => '0.1.0',
    'conflicts' => '',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author' => 'chrisitan.baer@publicispixelpark.de',
    'author_email' => '',
    'author_company' => 'Pixelpark',
    'constraints' => array(
        'depends' => array(
            'typo3' => '4.5-7.6.99',
            'cms' => '',
            'fluid' => '',
            'extbase' => '',
        ),
        'conflicts' => array(),
        'suggests' => array(),
    ),
    '_md5_values_when_last_written' => 'a:0:{}',
);
